require "rails_helper"

RSpec.describe Bid, type: :model do

  subject(:bid) { create(:bid) }

  it { is_expected.to belong_to(:client).with_foreign_key(:client_id).class_name('User') }
  it { is_expected.to delegate_method(:base_price).to(:property) }
  it { is_expected.to delegate_method(:owner).to(:property) }
  it { is_expected.to validate_presence_of(:client) }
  it { is_expected.to validate_presence_of(:property) }
  it { is_expected.to validate_numericality_of(:amount) }
  it { is_expected.to validate_uniqueness_of(:amount).scoped_to(:property_id) }

end
