require "rails_helper"
require "cancan/matchers"

RSpec.describe User, type: :model do

  it { is_expected.to have_many(:properties).with_foreign_key(:owner_id).inverse_of(:owner) }
  it { is_expected.to have_many(:bids).with_foreign_key(:client_id).inverse_of(:client) }

  describe "#profile_picture" do
    subject { create(:user, with_profile_picture: false).profile_picture }

    it { is_expected.to be_an_instance_of(ActiveStorage::Attached::One) }

    context 'when profile picture is not created' do
      subject { create(:user, with_profile_picture: false).profile_picture.attachment }

      it { is_expected.to be_nil }
    end
  end

  describe "#description" do
    context 'when description is empty' do
      subject { create(:user, description: "") }

      it 'should allow to save' do
        is_expected.to be_persisted
      end
    end

    context 'when description is nil' do
      subject { create(:user, description: nil) }    

      it 'should allow to save' do
        is_expected.to be_persisted
      end
    end
  end

  describe "#documents" do
    context 'when documents are created' do
      let (:expected_size) { 1 }
      subject { create(:user, documents_count: expected_size).documents }

      it { is_expected.to be_an_instance_of(ActiveStorage::Attached::Many) }
      it 'should have same number of documents' do
        expect(subject.size).to eql(expected_size)
      end
    end
  end

  describe "User" do
    describe "abilities" do
      subject(:ability) { Ability.new(user) }
      let(:user){ create(:user) }

      it { is_expected.to be_able_to(:create, Property ) }

      context "when property is owned by user (when user is owner)" do
        let(:user) { FactoryBot.create(:owner, properties_count: 1) }
        let(:property) { user.properties.first }

        it 'should be able to update his own property' do
          is_expected.to be_able_to(:update, property)
        end

        it 'should be able to destroy his own property' do
          is_expected.to be_able_to(:destroy, property)
        end

        it 'should be able to auction his own property' do
          is_expected.to be_able_to(:auction, property)
        end

        it 'should not be able to bid his own property' do
          is_expected.not_to be_able_to(:bid, property)
        end
      end

      context "when property is not owned by user (when user is client)" do
        let(:user) { FactoryBot.create(:client) }
        let(:property) { FactoryBot.create(:property) }

        it 'should not be able to update others property' do
          is_expected.not_to be_able_to(:update, property)
        end

        it 'should not be able to destroy others property' do
          is_expected.not_to be_able_to(:destroy, property)
        end

        it 'should not be able to auction others property' do
          is_expected.not_to be_able_to(:auction, property)
        end

        it 'should be able to bid others property' do
          is_expected.to be_able_to(:bid, property)
        end        
      end
    end
  end

end
