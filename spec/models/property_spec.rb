require "rails_helper"

RSpec.describe Property, type: :model do

  it { is_expected.to belong_to(:property_type) }
  it { is_expected.to belong_to(:owner).with_foreign_key(:owner_id).class_name('User') }
  it { is_expected.to have_many(:bids).inverse_of(:property) }
  it { is_expected.to validate_presence_of(:base_price) }
  it { is_expected.to validate_numericality_of(:base_price) }

  describe "#documents" do
    context 'when documents are created' do
      let (:expected_size) { 1 }
      subject { create(:property, documents_count: expected_size).documents }

      it { is_expected.to be_an_instance_of(ActiveStorage::Attached::Many) }
      it 'should have same number of documents' do
        expect(subject.size).to eql(expected_size)
      end
    end
  end

  describe "#images" do
    context 'when images are created' do
      let (:expected_size) { 1 }
      subject { create(:property, images_count: expected_size).images }

      it { is_expected.to be_an_instance_of(ActiveStorage::Attached::Many) }
      it 'should have same number of images' do
        expect(subject.size).to eql(expected_size)
      end
    end
  end

  describe "#place_bid" do
    context 'when proper arguments are passed' do
      let(:client) { create(:client) }
      subject { create(:property, base_price: 123).place_bid(client, 1235) }

      it 'should return persisted bid' do
        is_expected.to be_persisted
      end

      it { is_expected.to be_an_instance_of(Bid) }
    end

    context 'when bid value is less than base price' do
      let(:client) { create(:client) }
      subject { create(:property, base_price: 123).place_bid(client, 1) }

      it 'should return non persisted bid' do
        is_expected.not_to be_persisted
      end

      it { is_expected.to be_an_instance_of(Bid) }
    end

  end

end
