FactoryBot.define do
  factory :sale, class: 'PropertyType' do
    name { 'sale' }
  end

  factory :rent, class: 'PropertyType' do
    name { 'rent' }
  end
end
