FactoryBot.define do
  factory :property do
    transient do
      documents_count { 0 }
      images_count { 0 }
    end

    description { Faker::Lorem.paragraphs(rand(2..8)) }
    base_price { Faker::Number.decimal(4, 2) }
    association :owner, factory: :owner, strategy: :create
    association :property_type, factory: :rent, strategy: :create

    after(:create) do |property, evaluator|
      evaluator.documents_count.times do |index|
        property.documents.attach(
          io: File.open(Rails.root.join('spec', 'factories', 'images', 'profile_picture.png')),
          filename: File.basename("doccument_#{index + 1}.png")
        )
      end

      evaluator.images_count.times do |index|
        property.images.attach(
          io: File.open(Rails.root.join('spec', 'factories', 'images', 'profile_picture.png')),
          filename: File.basename("image_#{index + 1}.png")
        )
      end
    end

  end
end
