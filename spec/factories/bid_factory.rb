FactoryBot.define do
  factory :bid do
    association :client, factory: :user
    association :property, factory: :property
    amount { Faker::Number.decimal(5, 2) }
  end
end
