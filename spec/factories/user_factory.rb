FactoryBot.define do
  factory :user, aliases: [:client, :owner] do
    transient do
      properties_count { 0 }
      with_profile_picture { true }
      documents_count { 0 }
    end

    email { Faker::Internet.email }
    password { 'default_password' }
    password_confirmation { 'default_password' }
    description { Faker::Lorem.paragraphs(rand(2..8)) }

    after(:create) do |user, evaluator|
      create_list(:property, evaluator.properties_count, owner: user)

      user.profile_picture.attach(
        io: File.open(Rails.root.join('spec', 'factories', 'images', 'profile_picture.png')),
        filename: File.basename(Rails.root.join('spec', 'factories', 'images', 'profile_picture.png'))
      ) if evaluator.with_profile_picture

      evaluator.documents_count.times do |index|
        user.documents.attach(
          io: File.open(Rails.root.join('spec', 'factories', 'images', 'profile_picture.png')),
          filename: File.basename("doccument_#{index + 1}.png")
        )
      end
    end
  end
end
