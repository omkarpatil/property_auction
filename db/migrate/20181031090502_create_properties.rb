class CreateProperties < ActiveRecord::Migration[5.2]
  def change
    create_table :properties do |t|
      t.references :property_type, foreign_key: true
      t.text :description
      t.bigint :owner_id, index: :true
      t.decimal :base_price, precision: 8, scale: 2

      t.timestamps
    end
  end
end
