class CreateBids < ActiveRecord::Migration[5.2]
  def change
    create_table :bids do |t|
      t.references :property, foreign_key: true
      t.bigint :client_id, index: true
      t.decimal :amount, precision: 8, scale: 2

      t.timestamps
    end
  end
end
