class Property < ApplicationRecord
  has_many_attached :documents
  has_many_attached :images
  belongs_to :property_type
  belongs_to :owner, class_name: 'User', foreign_key: :owner_id
  has_many :bids, inverse_of: :property

  validates :base_price, presence: true
  validates :base_price, numericality: true

  def place_bid(user, amount)
    bid = self.bids.find_or_initialize_by(client: user)
    bid.amount = amount
    bid.tap { |b| b.save }
  end

end
