class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one_attached :profile_picture
  has_many_attached :documents

  has_many :properties, foreign_key: :owner_id, inverse_of: :owner
  has_many :bids, foreign_key: :client_id, inverse_of: :client

end
