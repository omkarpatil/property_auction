class Bid < ApplicationRecord
  # attribute :amount, :integer

  belongs_to :property
  belongs_to :client, class_name: 'User', foreign_key: :client_id

  validates :amount, :property, :client, presence: true
  validates :amount, uniqueness: { scope: :property_id }
  validates :amount, numericality: true
  validate :amount_cannot_be_less_than_base_price

  delegate :base_price, :owner, to: :property, allow_nil: true

  def amount_cannot_be_less_than_base_price
    if amount < base_price.to_f
      errors.add(:amount, "can't be less than base price")
    end
  end

end
